from setuptools import setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(name='multiwall',
      version='1.8',
      author='Avalon Parton',
      author_email='avalonlee@gmail.com',
      description='Wallpaper Getter and Setter',
      long_description=long_description,
      long_description_content_type="text/markdown",
      license='MIT',
      url='https://gitlab.com/avalonparton/multiwall',
      packages=['multiwall'],
      install_requires=['Pillow', 'python-unsplash', 'screeninfo'],
      zip_safe=False)
